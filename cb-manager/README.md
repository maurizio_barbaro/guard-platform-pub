# CB-Manager (Context Broker Manager)

[![License](https://img.shields.io/github/license/guard-project/cb-manager)](https://github.com/guard-project/cb-manager/blob/master/LICENSE)
[![Code size](https://img.shields.io/github/languages/code-size/guard-project/cb-manager?color=red&logo=github)](https://github.com/guard-project/cb-manager)
[![Repository Size](https://img.shields.io/github/repo-size/guard-project/cb-manager?color=red&logo=github)](https://github.com/guard-project/cb-manager)
[![Release](https://img.shields.io/github/v/tag/guard-project/cb-manager?label=release&logo=github)](https://github.com/guard-project/cb-manager/releases)
[![Docker image](https://img.shields.io/docker/image-size/guardproject/cb-manager?label=image&logo=docker)](https://hub.docker.com/repository/docker/guardproject/cb-manager)
[![Docs](https://readthedocs.org/projects/guard-cb-manager/badge/?version=latest)](https://guard-cb-manager.readthedocs.io)

## Contents

- [CB-Manager (Context Broker Manager)](#cb-manager-context-broker-manager)
  - [Contents](#contents)
  - [Installation Steps](#installation-steps)
    - [Setup](#setup)
    - [Requirements](#requirements)
    - [Initialization](#initialization)
    - [Start](#start)
    - [Stop](#stop)
    - [Health](#health)
  - [Docker image](#docker-image)
    - [Build](#build)
    - [Run](#run)
  - [References](#references)

The source code is available in the [src](github.com/guard-project/cb-manager) directory as git sub-module.

## Installation Steps

### Setup

The variables are defined in [scripts/vars](scripts/vars).

Name                              | Default value                                                         | Meaning
----------------------------------|-----------------------------------------------------------------------|--------
COMPONENT                         | cb-manager                                                            | Component name
VERSION                           | master                                                                | Component version
PROJECT                           | guard                                                                 | Project name
INSTALLATION_PATH                 | /opt/`$COMPONENT`                                                     | Destination path where the software will be installed
TMP_PATH                          | /tmp                                                                  | Temporary dictionary path
PIDFILE                           | `$TMP`/`$COMPONENT`.pid                                               | File path where the PID of the current execution is stored
CB_MAN_HOST                       | 0.0.0.0                                                               | Host address where CB-Manager is listening
CB_MAN_PORT                       | 5000                                                                  | TCP port where CB-Manager is listening
CB_MAN_AUTH                       | true                                                                  | Enable HTTP authentication
CB_MAN_HTTPS                      | false                                                                 | Force to use HTTPS instead of HTTP
CB_MAN_HEARTBEAT_TIMEOUT          | 10s                                                                   | Heartbeat timeout
CB_MAN_HEARTBEAT_PERIOD           | 1min                                                                  | Heartbeat period
CB_MAN_HEARTBEAT_AUTH_EXPIRATION  | 5min                                                                  | Heartbeat authentication time validity
CB_MAN_ELASTICSEARCH_ENDPOINT     | localhost:9200                                                        | Elasticsearch endpoint
CB_MAN_ELASTICSEARCH_TIMEOUT      | 20s                                                                   | Timeout for requests to Elasticsearch
CB_MAN_ELASTICSEARCH_RETRY_PERIOD | 1min                                                                  | Period of time to wait after which to retry connection with Elasticsearch
CB_MAN_ELASTIC_APM_ENABLED        | false                                                                 | Enable Elastic APM
CB_MAN_ELASTIC_APM_SERVER         | http://localhost:8200                                                 | Elastic APM Server
CB_MAN_DEV_USERNAME               | cb-manager                                                            | Username for HTTP authorization (used in development)
CB_MAN_DEV_PASSWORD               | a9d4034da07d8ef31db1cd4119b6a4552fdfbd19787e2848e71c8ee3b47703a7 [^1] | Password for HTTP authorization (used in development)
CB_MAN_LOG_LEVEL                  | DEBUG                                                                 | General LOG level

### Requirements

Enter into the `scripts` directory.

```console
$ cd scripts
```

### Initialization

```console
$ ./init
```

### Start

```console
$ ./start
```

### Stop

```console
$ ./stop
```

### Health

Check if the software is running or not.

```console
$ ./health
```

## Docker image

[Dockerfile](Dockerfile) is used to build the `docker` image with CI in the [https://hub.docker.com/repository/docker/guardproject/cb-manager](https://hub.docker.com/repository/docker/guardproject/cb-manager).

### Build

You can use [docker-build.sh](docker-build.sh) to build the image with tag guardproject/cb-manager:`$VERSION`.
`$VERSION` is the specific version to build the image.

```console
$ ./docker-build.sh <version>
```

Example:

```console
$ ./docker-build.sh master
```

### Run

In addition, it is possible to test the images with the [docker-run.sh](docker-run.sh) script setting the `$VERSION` variable with the command line argument like the [docker-build.sh](docker-build.sh) script.

```console
$ ./docker-run.sh <version>
```

Example:

```console
$ ./docker-run.sh master
```


## References

[^1] Password: "guard" hashed in sha256.
