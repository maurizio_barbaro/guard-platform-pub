# Changelog

## 1.0.0

- Initial release.
- Documentation in readthedocs.com (75%).
- Added XML and YAML content type support.
- Added XML and YAML parameter schema.

## 1.1.0
