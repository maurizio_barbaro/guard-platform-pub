#!/bin/bash
# GUARD - CB Manager
# author: Alex Carrega <alessandro.carrega@cnit.it>

set_var() {
    [ ! -v $1 ] && export $1="$2"
}

set_var COMPONENT cb-manager
set_var VERSION master
set_var PROJECT guard
set_var INSTALLATION_PATH "/opt/$COMPONENT"

set_var TMP_PATH /tmp
set_var PIDFILE "$TMP_PATH/$COMPONENT.pid"

set_var CB_MAN_HOST 0.0.0.0
set_var CB_MAN_PORT 5000
set_var CB_MAN_AUTH true
set_var CB_MAN_HTTPS false
set_var CB_MAN_HEARTBEAT_TIMEOUT 10s
set_var CB_MAN_HEARTBEAT_PERIOD 1min
set_var CB_MAN_HEARTBEAT_AUTH_EXPIRATION 5min
set_var CB_MAN_ELASTICSEARCH_ENDPOINT 10.0.0.6:9200
set_var CB_MAN_ELASTICSEARCH_TIMEOUT 20s
set_var CB_MAN_ELASTICSEARCH_RETRY_PERIOD 1min
set_var CB_MAN_ELASTIC_APM_ENABLED false
set_var CB_MAN_ELASTIC_APM_SERVER http://localhost:8200
set_var CB_MAN_DEV_USERNAME cb-manager
# Password: "guard" hashed in sha256
set_var CB_MAN_DEV_PASSWORD a9d4034da07d8ef31db1cd4119b6a4552fdfbd19787e2848e71c8ee3b47703a7
set_var CB_MAN_LOG_LEVEL DEBUG
